package org.firstinspires.ftc.teamcode.exemples;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

@TeleOp(name="Test Fonctionnement")
public class TestFonctionnement extends OpMode
{
    private ElapsedTime runtime = new ElapsedTime();

    /*
     * Tout ce qui est dans "init()" sera exécuté une fois lors de l'initialisation du programme
     */
    @Override
    public void init() {
        telemetry.addLine("Robot initialisé");
        telemetry.update();
    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }

    /*
     * Sera exécuté en boucle une fois que le programme est lancé
     */
    @Override
    public void loop() {
        telemetry.addData("Temps depuis le start", runtime.time()); // On affiche le temps qui s'est écoulé depuis qu'on a appuyé sur play
        telemetry.update();
    }

    @Override
    public void stop() {
        telemetry.addLine("Robot arrété");
        telemetry.update();
    }

}
