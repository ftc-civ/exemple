package org.firstinspires.ftc.teamcode.jour2;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

@TeleOp(name="Mode manuel test Gamepad",group="Jour2")
public class ManuelTestGamepad extends OpMode
{
  // Membres de la classe (moteurs, capteurs, variables...)
  private ElapsedTime runtime = new ElapsedTime();
  private boolean moteurBoutonA = false;
  private boolean moteurBoutonB = false;
  private boolean longPressA = false;
  private boolean longPressB = false;

  // A DECOMMENTER
  // private GamepadController gamepad;

  @Override
  public void init() {
    // A DECOMMENTER
    // gamepad = new GamepadController(telemetry, gamepad1);
  }

  @Override
  public void init_loop() {
  }

  @Override
  public void start() {
    runtime.reset();
  }

  /*
   * Sera exécuté en boucle une fois que le programme est lancé
   */
  @Override
  public void loop() {
    // A DECOMMENTER
    /* if (gamepad.press("a")) {
      moteurBoutonA = !moteurBoutonA;
    }
    if (gamepad.press("b")) {
      moteurBoutonB = !moteurBoutonB;
    }
    longPressA = gamepad.longPress("a");
    longPressB = gamepad.longPress("b"); */

    telemetry.addData("Moteur a", moteurBoutonA);
    telemetry.addData("Moteur b", moteurBoutonB);
    telemetry.addData("Longpress a", longPressA);
    telemetry.addData("Longpress b", longPressB);
    telemetry.update();
  }

  @Override
  public void stop() {
  }

}
