package org.firstinspires.ftc.teamcode.exemples;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

@TeleOp(name="Exemple mode autonome")
@Disabled
public class ExempleAuto extends LinearOpMode {

    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor moteurGauche;
    private DcMotor moteurDroite;
    private DistanceSensor capteurDistance;

    @Override
    public void runOpMode() {
        moteurGauche  = hardwareMap.get(DcMotor.class, "moteur_gauche");
        moteurDroite = hardwareMap.get(DcMotor.class, "moteur_droite");
        capteurDistance = hardwareMap.get(DistanceSensor.class, "capteur_distance");

        // On attends jusqu'à ce qu'on appuie sur Play sur le téléphone
        waitForStart();
        runtime.reset();

        while (capteurDistance.getDistance(DistanceUnit.CM) > 20) {
            setPower(1.0); // On avance tant qu'il reste plus de 20cm devant le robot

            telemetry.addData("Distance", capteurDistance.getDistance(DistanceUnit.CM));
            telemetry.update();
        }
        setPower(0.0); // On s'arrete quand il y a moins de 20cm

        tourner(0.5); // On tourne à la moitié de la puissance dispo
        double tempsAuDepart = runtime.time();
        while (runtime.time() - tempsAuDepart < 1000); // On attends 1 seconde (1000ms)
        setPower(0.0); // On s'arrete
    }

    // On définit des méthodes (fonctions) pour organiser le programme, notamment si on répète souvent ces lignes
    void setPower(double puissance) {
        moteurGauche.setPower(puissance);
        moteurDroite.setPower(puissance);
    }

    void tourner(double puissance) {
        moteurGauche.setPower(puissance);
        moteurDroite.setPower(-puissance);
    }
}
