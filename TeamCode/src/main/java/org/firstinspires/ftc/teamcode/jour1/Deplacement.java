package org.firstinspires.ftc.teamcode.jour1;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.GyroscopeWrapper;

@TeleOp(name="Activite deplacement", group="Jour1")
public class Deplacement extends OpMode
{
    // Membres de la classe (moteurs, capteurs, variables...)
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor moteurAvantGauche, moteurAvantDroite, moteurArriereGauche, moteurArriereDroite;
    private GyroscopeWrapper gyroscope;

    /*
     * Tout ce qui est dans "init()" sera exécuté une fois lors de l'initialisation du programme
     */
    @Override
    public void init() {
        moteurAvantGauche = hardwareMap.get(DcMotor.class, "front_left_drive");
        moteurAvantDroite = hardwareMap.get(DcMotor.class, "front_right_drive");
        moteurArriereGauche = hardwareMap.get(DcMotor.class, "back_left_drive");
        moteurArriereDroite = hardwareMap.get(DcMotor.class, "back_right_drive");
        gyroscope = new GyroscopeWrapper(hardwareMap.get(BNO055IMU.class, "imu"));

        moteurAvantGauche.setDirection(DcMotorSimple.Direction.REVERSE);
        moteurAvantDroite.setDirection(DcMotorSimple.Direction.REVERSE);
        moteurArriereGauche.setDirection(DcMotorSimple.Direction.FORWARD);
        moteurArriereDroite.setDirection(DcMotorSimple.Direction.REVERSE);

        telemetry.addLine("Robot initialisé");
        telemetry.update();
    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }

    /*
     * Sera exécuté en boucle une fois que le programme est lancé
     */
    @Override
    public void loop() {
        ///////// METTEZ VOTRE CODE ICI //////////


        telemetry.update();
    }

    @Override
    public void stop() {
    }

}
