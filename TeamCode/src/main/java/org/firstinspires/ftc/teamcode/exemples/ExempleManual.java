package org.firstinspires.ftc.teamcode.exemples;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

@TeleOp(name="Exemple mode manuel")
@Disabled
public class ExempleManual extends OpMode
{
    // Membres de la classe (moteurs, capteurs, variables...)
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor moteur1 = null;
    private DcMotor moteur2 = null;
    private DistanceSensor capteurDistance = null;

    /*
     * Tout ce qui est dans "init()" sera exécuté une fois lors de l'initialisation du programme
     */
    @Override
    public void init() {
        // On utilise hardwareMap.get(Type.class, "cle") pour récupérer les moteurs, capteurs, etc. Il faudra renseigner la valeur que vous mettez dans "cle" dans la config, sur le téléphone.
        moteur1 = hardwareMap.get(DcMotor.class, "moteur_1");
        moteur2 = hardwareMap.get(DcMotor.class, "moteur_2");
        capteurDistance = hardwareMap.get(DistanceSensor.class, "capteur_distance");

        // Vous pouvez utiliser telemetry.addData("nom", variable); ou telemetry.addLine("message"); pour afficher des informations sur le téléphone
        telemetry.addLine("Robot initialisé");
        telemetry.update();
    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }

    /*
     * Sera exécuté en boucle une fois que le programme est lancé
     */
    @Override
    public void loop() {
        if (capteurDistance.getDistance(DistanceUnit.CM) > 30) {    // Si il y a plus de 30cm devant le capteur de distance
            moteur1.setPower(1.0);                                  // Allumer le moteur 1 à pleine puissance
        } else {                                                    // Sinon (si il y a moins de 30cm)
            moteur1.setPower(0.0);                                  // Éteindre le moteur
        }

        moteur2.setPower(gamepad1.left_stick_y);                    // Allumer le moteur 2 en fonction de la valeur du joystick gauche

        telemetry.addData("Temps depuis le début", runtime.time()); // On affiche le temps qui s'est écoulé depuis qu'on a appuyé sur play
        telemetry.update();
    }

    @Override
    public void stop() {
    }

}
