package org.firstinspires.ftc.teamcode.jour1;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.SimpleMovement;

@TeleOp(name="Activite Capteur de Couleur", group="Jour1")
@Disabled
public class Couleur extends LinearOpMode {

  private ElapsedTime runtime = new ElapsedTime();
  private SimpleMovement movement;
  private ColorSensor capteurGauche;
  private ColorSensor capteurDroite;

  @Override
  public void runOpMode() {
    movement = new SimpleMovement(
            hardwareMap.get(DcMotor.class, "moteur_gauche"),
            hardwareMap.get(DcMotor.class, "moteur_droite")
    );
    capteurGauche = hardwareMap.get(ColorSensor.class, "capteur_gauche");
    capteurDroite = hardwareMap.get(ColorSensor.class, "capteur_droite");

    waitForStart();
    runtime.reset();

    ////////// METTEZ VOTRE CODE EN DESSOUS //////////

  }
}
