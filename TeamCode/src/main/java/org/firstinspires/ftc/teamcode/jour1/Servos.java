package org.firstinspires.ftc.teamcode.jour1;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

@TeleOp(name="Activite servos", group="Jour1")
public class Servos extends OpMode
{
    // Membres de la classe (moteurs, capteurs, variables...)
    private ElapsedTime runtime = new ElapsedTime();
    private Servo servo1;
    private Servo servo2;
    private Servo servo3;

    /*
     * Tout ce qui est dans "init()" sera exécuté une fois lors de l'initialisation du programme
     */
    @Override
    public void init() {
        servo1 = hardwareMap.get(Servo.class, "servo_1");
        servo2 = hardwareMap.get(Servo.class, "servo_2");
        servo3 = hardwareMap.get(Servo.class, "servo_3");

        telemetry.addLine("Robot initialisé");
        telemetry.update();
    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }

    /*
     * Sera exécuté en boucle une fois que le programme est lancé
     */
    @Override
    public void loop() {
        ///////// METTEZ VOTRE CODE ICI //////////


        telemetry.update();
    }

    @Override
    public void stop() {
    }

}
