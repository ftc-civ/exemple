package org.firstinspires.ftc.teamcode.jour1;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.teamcode.util.FrenchineersMovement;

@TeleOp(name="Activite Capteur de Distance", group="Jour1")
public class Distance extends LinearOpMode {

    private ElapsedTime runtime = new ElapsedTime();
    private FrenchineersMovement movement;
    private DistanceSensor capteurGauche;
    private DistanceSensor capteurDroite;
    private DistanceSensor capteurAvant;

    @Override
    public void runOpMode() {
        movement = new FrenchineersMovement(
                hardwareMap.get(DcMotor.class, "moteur_avant_gauche"),
                hardwareMap.get(DcMotor.class, "moteur_avant_droite"),
                hardwareMap.get(DcMotor.class, "moteur_arriere_gauche"),
                hardwareMap.get(DcMotor.class, "moteur_arriere_droite"),
                hardwareMap.get(BNO055IMU.class, "imu")
        );
        capteurGauche = hardwareMap.get(DistanceSensor.class, "capteur_gauche");
        capteurDroite = hardwareMap.get(DistanceSensor.class, "capteur_droite");
        capteurAvant = hardwareMap.get(DistanceSensor.class, "capteur_avant");

        waitForStart();
        runtime.reset();

        ////////// METTEZ VOTRE CODE EN DESSOUS //////////


    }
}
