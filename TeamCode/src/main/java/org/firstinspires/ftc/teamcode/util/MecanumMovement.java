/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.util;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.Telemetry;

@Config
public class MecanumMovement
{
    // Initialize motors
    private DcMotor frontLeftDrive, frontRightDrive, backLeftDrive, backRightDrive;

    ///////////* CONSTRUCTOR *///////////
    public MecanumMovement(DcMotor FL, DcMotor FR, DcMotor BL, DcMotor BR) {
        // Assign motors from ManualOpMode
        frontLeftDrive  = FL; frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive = FR; frontRightDrive.setDirection(DcMotor.Direction.REVERSE);
        backLeftDrive  = BL;  backLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        backRightDrive = BR;  backRightDrive.setDirection(DcMotor.Direction.REVERSE);


    }

    ///////////* MOVEMENT CALCULATIONS *///////////
    // Move in a direction indicated by the first two coordinates, and turn
    public void move(double front, double sideways, double turn) {
        backLeftDrive.setPower(Range.clip(front + sideways - turn, -1.0, 1.0));
        backRightDrive.setPower(Range.clip(front - sideways + turn, -1.0, 1.0));
        frontRightDrive.setPower(Range.clip(front + sideways + turn, -1.0, 1.0));
        frontLeftDrive.setPower(Range.clip(front - sideways - turn, -1.0, 1.0));
    }

    // Move with an angle and a speed instead
    public void polarMove(double angle, double speed, double turn) {
        move(
                Math.sin(angle) * speed,
                Math.cos(angle) * speed,
                turn
        );
    }


}
